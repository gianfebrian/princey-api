# PRINCEY API #

[![pipeline status](https://gitlab.com/gianfebrian/princey-api/badges/master/pipeline.svg)](https://gitlab.com/gianfebrian/princey-api/-/commits/master)
[![coverage report](https://gitlab.com/gianfebrian/princey-api/badges/master/coverage.svg)](https://gitlab.com/gianfebrian/princey-api/-/commits/master)

Princey API is a price monitoring service which fetches a product information from any sites. It uses scheduler and crawler to visit the product page at certain interval and re-fetch it to get the latest price. Scheduler module uses mongodb as its persistent storage and provides application level OCC to be used in distributed systems. To be able to crawl modern sites, Crawler module has 2 engines, html for plain old html page and headless for javascript sites. Both Scheduler and Crawler have its own API to interact with them. Since crawling process can be exhaustive, Crawler API uses its own simple queue mechanic with mongodb as its persitent storage.

Before run the app, you need to configure the app via environment variables or `.env` file. Simply, rename `.env.example` to `.env` and set `MONGODB_URI`, it is the connection string of your mongodb instance.

To run the app:
```
npm start
```

To test it:
```
npm test
```

Because environment variables can introduce polution and for scability in mind, if you want to run it on a containerized env, build docker image with:
```
docker build -t princeyapi .
```

And run it:
```
docker run -it --rm --env-file [PATH/TO/YOUR/ENV/FILE] princeyapi
```



## THE NEAREST MILESTONES (TODO) ##
* Move independent modules crawler, crawler-rest-api, scheduler, scheduler-rest-api, and simple-queue onto separated repos or use monorepo (lerna)
* Protect endpoints
