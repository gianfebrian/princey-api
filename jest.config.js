require('dotenv').config();

module.exports = {
  verbose: true,
  collectCoverage: true,
  coverageReporters: ['text-summary'],
  collectCoverageFrom: [
    'src/**/*.{js,jsx}',
    '!**/node_modules/**',
    '!**/vendor/**',
  ],
  testEnvironment: 'node',
};
