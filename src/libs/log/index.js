const _ = require('lodash');
const { createLogger, format, transports } = require('winston');

const formatter = (logFormat) => {
  let output;
  switch (logFormat) {
    case 'json':
      output = format.json();
      break;
    case 'simple':
      output = format.simple();
      break;
    default:
      output = format.simple();
      break;
  }

  return format.combine(
    format.errors({ stack: true }),
    output,
  );
};

const isSilent = (silent) => (_.isEmpty(silent) ? true : _.toUpper(silent) === 'TRUE');

const log = createLogger({
  level: process.env.LOG_LEVEL || 'debug',
  silent: isSilent(process.env.LOG_SILENT),
  format: formatter(process.env.LOG_FORMAT),
  transports: [new transports.Console()],
});

const overrideStream = () => {
  log.stream = {
    write: (message) => {
      log.info(message);
    },
  };
};

module.exports = {
  log,
  formatter,
  isSilent,
  overrideStream,
};
