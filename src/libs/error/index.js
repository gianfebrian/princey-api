const toObject = (error) => (
  Object.getOwnPropertyNames(error)
    .reduce((acc, v) => {
      const out = { ...acc };
      out[v] = error[v];

      return out;
    }, {})
);

const toJSON = (error) => (
  JSON.stringify(toObject(error))
);

const getInternalErrorObject = () => ({
  message: 'Internal server error',
});

class ApplicationError extends Error {
  constructor(message, code) {
    super(message);
    if (code) this.code = code;
    Error.captureStackTrace(this, this.constructor);
  }

  getMessage() {
    return { message: this.message };
  }

  toJSON(isProduction) {
    return toJSON(isProduction ? this.getMessage() : this);
  }

  toObject(isProduction) {
    return toObject(isProduction ? this.getMessage() : this);
  }
}

module.exports = {
  toJSON,
  toObject,
  getInternalErrorObject,
  ApplicationError,
};
