
/**
 * e.g:
 * title: [space][space]Lorem Ipsum 123[space][space]4[space][space]
 *
 * with [space] indicates leading/trailing space
 *
 * expected output:
 * title: Lorem Ipsum 123 4
 *
 */
exports.sanitizeSpace = (str) => (
  str && str
    .trim()
    .split(' ')
    .map((v) => v.trim())
    .join(' ')
);

/**
 * e.g:
 * title: [space][space]Lorem Ipsum 123 4[space][space][space]
 *
 * with [space] indicates leading/trailing space
 *
 * expected output:
 * slug: lorem-ipsum-123-4
 *
*/
exports.sluggify = (str) => (
  str && str
    .toLowerCase()
    .trim()
    .split(' ')
    .map((v) => v.trim())
    .join('-')
);
