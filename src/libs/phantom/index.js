
/**
 * assuming we have: const tuple = { a: 'some value', b: undefined }
 * but we don't want any empty value from any property to be processed
 * in later stage. `cleanupObject` will remove such case from the tuple.
 *
 * this may come in handy when we're processing phantom record (dirty)
 * from the client which doesn't send complete pairs or
 * send the complete pairs with some unset values.
 *
 */
exports.cleanupObject = (data) => (
  Object.fromEntries(
    Object.entries(data)
      .reduce((r, v) => (
        (typeof v[1] !== 'undefined' && v[1] !== null) ? r.concat([v]) : r
      ), []),
  )
);
