
const moment = require('moment');

/**
 * now represents utc datetime in unix milis format
*/
exports.now = () => moment().utc().valueOf();
