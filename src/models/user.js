
const { model, Schema } = require('mongoose');

const userSchema = new Schema({
  fullname: { type: String },
  username: { type: String },
  slug: { type: String },
  avatar: { type: String },
  email: { type: String },
  password: { type: String },
  createdAt: { type: Number },
  deletedAt: { type: Number },
  updatedAt: { type: Number },
});

const User = model('User', userSchema);

module.exports = { User, userSchema };
