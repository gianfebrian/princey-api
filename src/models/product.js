
const { model, Schema } = require('mongoose');

const productSchema = new Schema({
  url: { type: String },
  name: { type: String },
  description: { type: String },
  images: [{ type: String }],
  priceHistory: [
    {
      price: { type: Number },
      timestamp: { type: Number },
    },
  ],
  latestPrice: { type: Number },
  latestPriceText: { type: String },
  timestamp: { type: Number },
});

const Product = model('Product', productSchema);

module.exports = { Product, productSchema };
