
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userRepo = require('./user');
const { User } = require('./../models/user');

exports.register = async (data) => {
  const user = { ...data };
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(user.password, salt);
  user.password = hash;

  return userRepo.create(user);
};

exports.signIn = async ({ email, password }) => {
  const user = await User.findOne({ email });
  const valid = await bcrypt.compare(password, user.password);
  if (!valid) {
    throw new Error('invalid email or password');
  }

  const payload = {
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(payload, process.env.SECRET_KEY);
};

exports.changePassword = async ({ email, oldPassword, newPassword }) => {
  const user = await User.findOne({ email });
  const valid = await bcrypt.compare(oldPassword, user.password);
  if (!valid) {
    throw new Error('invalid old passowrd');
  }

  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(newPassword, salt);

  return User.updateOne({ email }, { password: hash });
};

exports.isAuth = (token) => {
  try {
    const decoded = jwt.verify(token, process.env.SECRET_KEY);
    return decoded;
  } catch (err) {
    throw new Error('invalid auth token');
  }
};

exports.isAdmin = (decoded) => decoded.isAdmin;
