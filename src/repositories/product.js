const { sorter, pager } = require('./aggregation');
const { Product } = require('./../models/product');
const datetime = require('./../libs/datetime');
const stringer = require('./../libs/stringer');

exports.upsert = async ({
  url,
  name,
  description,
  price,
  images,
}) => {
  const sanitizedUrl = stringer.sanitizeSpace(url);
  const sanitizedName = stringer.sanitizeSpace(name);
  const now = datetime.now();

  const currentPrice = {
    price,
    timestamp: now,
  };

  return Product.updateOne(
    {
      url: sanitizedUrl,
    },
    {
      description,
      url: sanitizedUrl,
      name: sanitizedName,
      latestPrice: price,
      latestPriceText: String(price || ''),
      images,
      $push: { priceHistory: currentPrice },
      timestamp: now,
    },
    {
      upsert: true,
    },
  );
};

exports.list = async ({
  page = 1,
  rowsPerPage = 5,
  sortBy = '_id',
  sortDir = 'desc',
  filter,
}) => {
  const aggregation = [
    sorter(sortBy, sortDir),
    ...pager(page, rowsPerPage),
  ];

  if (filter && filter.length > 2) {
    const matcher = { $match: {} };
    const fields = ['name', 'url', 'description', 'latestPriceText'];
    matcher.$match.$or = fields.map((f) => ({ [f]: new RegExp(`${filter}`, 'ig') }));
    aggregation.unshift(matcher);
  }

  const data = await Product.aggregate(aggregation);

  return data[0] || { rows: [], totalCount: 0 };
};

exports.get = async (_id) => Product.findOne({ _id });
