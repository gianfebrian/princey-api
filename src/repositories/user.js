
const { sorter, pager } = require('./aggregation');
const { User } = require('./../models/user');
const datetime = require('./../libs/datetime');
const stringer = require('./../libs/stringer');
const phantom = require('./../libs/phantom');

exports.create = async ({
  fullname,
  username,
  slug,
  avatar,
  email,
  password,
}) => {
  const sanitizedFullname = stringer.sanitizeSpace(fullname);
  const sanitizedUsername = stringer.sanitizeSpace(username);
  const sluggified = stringer.sluggify(slug || sanitizedUsername);
  const now = datetime.now();

  return User.create({
    avatar,
    email,
    password,
    fullname: sanitizedFullname,
    username: sanitizedUsername,
    slug: sluggified,
    createdAt: now,
  });
};

exports.update = async (_id, {
  fullname,
  username,
  slug,
  avatar,
  email,
  password,
}) => {
  const sanitizedFullname = stringer.sanitizeSpace(fullname);
  const sanitizedUsername = stringer.sanitizeSpace(username);
  const sluggified = stringer.sluggify(slug || sanitizedUsername);

  const now = datetime.now();

  return User.updateOne({ _id }, phantom.cleanupObject({
    avatar,
    email,
    password,
    fullname: sanitizedFullname,
    username: sanitizedUsername,
    slug: sluggified,
    updatedAt: now,
  }));
};

exports.trash = async (ids = []) => {
  const now = datetime.now();

  return User.updateMany({ _id: { $in: ids } }, { deletedAt: now });
};

exports.list = async ({
  page = 1,
  rowsPerPage = 10,
  sortBy = '_id',
  sortDir = 'desc',
  filter = {},
}) => {
  const aggregation = [
    sorter(sortBy, sortDir),
    ...pager(page, rowsPerPage),
  ];

  const matcher = {
    $match: {
      deletedAt: null,
    },
  };

  if (filter && filter.length > 2) {
    const fields = ['fullname', 'username', 'slug', 'email'];
    matcher.$match.$or = fields.map((f) => ({ [f]: new RegExp(`${filter}`, 'i') }));
  }

  aggregation.unshift(matcher);

  const data = await User.aggregate(aggregation);

  return data[0];
};

exports.get = async (_id) => User.findOne({ _id, deletedAt: null });
