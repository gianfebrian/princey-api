
exports.sorter = (sortBy, sortDir) => ({
  $sort: { [sortBy]: sortDir.toUpperCase() === 'ASC' ? 1 : -1 },
});

exports.pager = (page = 1, rowsPerPage = 10) => ([
  {
    $facet: {
      rows: [
        {
          $skip: (Number(page) - 1) * Number(rowsPerPage),
        },
        {
          $limit: Number(rowsPerPage),
        },
      ],
      totalCount: [
        {
          $count: 'count',
        },
      ],
    },
  },
  { $unwind: '$totalCount' },
  {
    $project: {
      rows: '$rows',
      totalCount: '$totalCount.count',
    },
  },
]);
