
const repository = require('./../repositories/auth');

const isAuth = async (authorization) => {
  if (!authorization) {
    throw new Error('invalid token');
  }
  return repository.isAuth(authorization.replace('Bearer ', ''));
};

exports.auth = async (ctx, next) => {
  try {
    await isAuth(ctx.request.headers.authorization);
    return next();
  } catch (err) {
    ctx.status = 400;
    throw err;
  }
};

exports.isAdmin = async (ctx, next) => {
  try {
    const decoded = await isAuth(ctx.request.headers.authorization);
    if (!decoded.isAdmin) {
      throw new Error('restricted access');
    }
    return next();
  } catch (err) {
    ctx.status = 400;
    throw err;
  }
};
