const {
  toObject,
  getInternalErrorObject,
  ApplicationError,
} = require('./../libs/error');

exports.error = async (ctx, next) => {
  await next();

  if (ctx.state.error) {
    ctx.app.emit('error', ctx.state.error);

    const response = { error: toObject(ctx.state.error) };
    if (process.env.NODE_ENV === 'production') {
      response.error = getInternalErrorObject();
      if (ctx.state.error instanceof ApplicationError) {
        response.error = ctx.state.error.toObject(true);
      }
    }

    ctx.body = response;
  }
};
