
const repository = require('./../repositories/product');

exports.upsert = async (ctx, next) => {
  try {
    await repository.upsert(ctx.request.body);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.list = async (ctx, next) => {
  try {
    const data = await repository.list(ctx.request.query);
    ctx.status = 200;
    ctx.body = data;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.get = async (ctx, next) => {
  try {
    const data = await repository.get(ctx.params._id); // eslint-disable-line
    ctx.status = 200;
    ctx.body = data;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};
