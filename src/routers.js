const Router = require('koa-router');

const monit = require('./apis/monit');
const product = require('./apis/product');

const monitRouter = new Router({ prefix: '/monit' });
monitRouter.get('/ping', monit.ping);

const productRouter = new Router({ prefix: '/products' });
productRouter.get('/', product.list);
productRouter.get('/:_id', product.get);
productRouter.post('/', product.upsert);

const router = new Router();
router.use(monitRouter.routes());
router.use(productRouter.routes());

module.exports = router;
