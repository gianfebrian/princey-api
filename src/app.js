const http = require('http');
const Koa = require('koa');
const cors = require('@koa/cors');
const logger = require('koa-logger');
const bodyParser = require('koa-bodyparser');
const mongoose = require('mongoose');

const SimpleQueue = require('./services/simple-queue');
const Crawler = require('./services/crawler');
const CrawlerRestApi = require('./services/crawler-rest-api/koa');
const CrawlerRestApiRepo = require('./services/crawler-rest-api/repository');
const Scheduler = require('./services/scheduler');
const SchedulerRestApi = require('./services/scheduler-rest-api/koa');

const middlewareError = require('./middlewares/error');

const { log } = require('./libs/log');
const routers = require('./routers');

const app = new Koa();

// register global middlewares
app.on('error', (err) => log.error(err));
app.on('info', (info) => log.info(info));
app.on('debug', (debug) => log.debug(debug));
app.use(logger({ transporter: (str) => log.info(str) }));
app.use(bodyParser());
app.use(cors({ origin: '*', allowMethods: null }));
app.use(middlewareError.error);

const scheduler = new Scheduler({
  options: {
    maxLockWait: 10 * 60 * 1000, // 10 minutes
    loopInterval: 1 * 60 * 1000, // maximum job late execution is ~1 minute
    connection: mongoose.connection,
  },
  logger: log,
});

const schedulerMiddleware = SchedulerRestApi.middleware(scheduler);
const schedulerRouter = SchedulerRestApi.router([
  schedulerMiddleware,
]);
app.use(schedulerRouter.routes());

const crawler = new Crawler({
  options: { connection: mongoose.connection },
});

const simpleQueue = new SimpleQueue({
  options: { connection: mongoose.connection },
});

const crawlerMiddleware = CrawlerRestApi.middleware(crawler, simpleQueue);
const crawlerRouter = CrawlerRestApi.router([
  crawlerMiddleware,
]);
app.use(crawlerRouter.routes());

// register router middleware
app.use(routers.routes());

(async () => {
  try {
    const connectionOption = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    };

    await mongoose.connect(process.env.MONGODB_URI, connectionOption);
    log.info('connected to mongo');

    CrawlerRestApiRepo.fetchNext(crawler, simpleQueue);
    app.emit('ready');
  } catch (err) {
    app.emit('error', err);
  }
})();

// create http listener
const server = http.createServer(app.callback());

module.exports = { app, server };
