
const fetch = require('node-fetch');

exports.list = (crawler) => (
  crawler.listConfig()
);

exports.get = (crawler, name) => (
  crawler.getConfigByName(name)
);

exports.upsert = (crawler, config) => (
  crawler.setConfig(config)
);

exports.delete = (crawler, name) => (
  crawler.unsetConfigByName(name)
);

exports.delegate = async (crawler, queue, { url, callback = {} }) => {
  await queue.push({ url, callback });
  this.fetchNext(crawler, queue);
};

const delegationStatus = { fetching: false, next: false };

exports.fetchNext = async (crawler, queue) => {
  const { fetching, next } = delegationStatus;
  if (fetching && !next) {
    return;
  }

  const queueContext = queue.createNewContext();
  try {
    const current = await queueContext.pull();
    if (!current) {
      delegationStatus.fetching = false;
      delegationStatus.next = false;
      return;
    }

    await this.crawl(crawler, current.data);
    await queueContext.commit();
  } catch (err) {
    await queueContext.rollback();
  } finally {
    delegationStatus.next = true;
  }

  await this.fetchNext(crawler, queue);
};

exports.crawl = async (crawler, { url, callback = {} }) => {
  const config = await crawler.getConfigByUrl(url);
  const output = await crawler.fetch(url, config);
  const data = { ...output, url };

  if (callback.url) {
    const options = {
      timeout: 1000,
      ...callback.requestOptions,
      method: 'post',
      body: JSON.stringify(data),
    };

    options.headers = {
      ...options.headers,
      'Content-Type': 'application/json',
    };

    await fetch(callback.url, options);
  }

  return data;
};
