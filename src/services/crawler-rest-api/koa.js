
const Router = require('koa-router');

const repository = require('./repository');

exports.middleware = (crawler, queue) => (ctx, next) => {
  ctx.queue = queue;
  ctx.crawler = crawler;
  return next();
};

exports.list = async (ctx, next) => {
  try {
    const data = await repository.list(ctx.crawler);
    ctx.status = 200;
    ctx.body = data;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.get = async (ctx, next) => {
  try {
    const data = await repository.get(ctx.crawler, ctx.params.name);
    ctx.status = 200;
    ctx.body = data || {};
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.upsert = async (ctx, next) => {
  try {
    await repository.upsert(ctx.crawler, ctx.request.body);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.delete = async (ctx, next) => {
  try {
    await repository.delete(ctx.crawler, ctx.params.name);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.crawl = async (ctx, next) => {
  try {
    await repository.delegate(ctx.crawler, ctx.queue, ctx.request.body);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.router = (middlewares) => {
  const router = new Router({ prefix: '/crawlers' });
  router.use(middlewares);

  router.get('/', this.list);
  router.get('/:name', this.get);
  router.post('/', this.upsert);
  router.delete('/:name', this.delete);
  router.post('/crawl', this.crawl);

  return router;
};
