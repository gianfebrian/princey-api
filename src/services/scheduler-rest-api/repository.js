
const fetcher = require('./fetch');

exports.list = (scheduler) => (
  scheduler.hook.store.find({})
);

exports.get = (scheduler, name) => (
  scheduler.hook.store.findOne({ name })
);

exports.upsert = (scheduler, { name, repeatInterval, data }) => {
  scheduler.create(name, fetcher.exec().bind(fetcher));
  scheduler.every(name, repeatInterval, data);

  return Promise.all(scheduler.timers);
};

exports.delete = (scheduler, name) => (
  scheduler.hook.store.deleteOne({ name })
);

exports.start = async (scheduler) => {
  const persistedJobs = await scheduler.hook.store.find({});
  persistedJobs.forEach((job) => {
    scheduler.create(job.name, fetcher.exec().bind(fetcher));
    scheduler.every(job.name, job.repeatInterval, job.data);
  });

  return scheduler.start();
};

exports.stop = (scheduler) => scheduler.stop();

exports.status = (scheduler) => {
  const schedulerWorkerName = scheduler.processor.options.workerName || 'Default';
  const isSchedulerRunning = !!scheduler.running;
  const isProcessing = scheduler.processor.isProcessing; // eslint-disable-line
  const totalRunningMiles = scheduler.processor.nthRunner;
  const totalJobsInMemory = scheduler.registry.jobs.length;

  return {
    schedulerWorkerName,
    isSchedulerRunning,
    isProcessing,
    totalRunningMiles,
    totalJobsInMemory,
  };
};
