
const fetch = require('node-fetch');

exports.exec = (fetcher = fetch) => ({ data }) => {
  let dataUrl = data.url;
  if (data.params) {
    Object.entries(data.params)
      .forEach((p) => {
        dataUrl = dataUrl.replace(`:${p[0]}`, p[1]);
      });
  }

  const url = new URL(dataUrl);
  if (data.query) {
    Object.entries(data.query)
      .forEach((q) => url.searchParams.append(q[0], q[1]));
  }

  const options = {
    timeout: data.timeout || 1000,
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  if (data.headers) {
    options.headers = { ...options.headers, ...data.headers };
  }

  if (data.method) {
    options.method = data.method.toLowerCase();
  }

  if (data.body) {
    options.body = JSON.stringify(data.body);
  }

  return fetcher(url, options);
};
