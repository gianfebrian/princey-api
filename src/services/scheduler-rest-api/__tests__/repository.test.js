require('./../fetch');

const repository = require('./../repository');

jest.mock('./../fetch', () => ({
  exec: () => jest.fn(),
}));

describe('repository', () => {
  test('list', () => {
    const scheduler = {
      hook: {
        store: {
          find: jest.fn(),
        },
      },
    };

    repository.list(scheduler);
    expect(scheduler.hook.store.find).toHaveBeenCalled();
  });

  test('get', () => {
    const scheduler = {
      hook: {
        store: {
          findOne: jest.fn(),
        },
      },
    };

    repository.get(scheduler);
    expect(scheduler.hook.store.findOne).toHaveBeenCalled();
  });

  test('upsert', async () => {
    const scheduler = {
      create: jest.fn(),
      every: jest.fn(),
      timers: [Promise.resolve()],
    };

    await repository.upsert(scheduler, {});
    expect(scheduler.create).toHaveBeenCalled();
    expect(scheduler.every).toHaveBeenCalled();
  });

  test('delete', async () => {
    const scheduler = {
      hook: {
        store: {
          deleteOne: jest.fn(),
        },
      },
    };

    await repository.delete(scheduler);
    expect(scheduler.hook.store.deleteOne).toHaveBeenCalled();
  });

  test('start', async () => {
    const scheduler = {
      hook: {
        store: {
          find: jest.fn(),
        },
      },
      create: jest.fn(),
      every: jest.fn(),
      start: jest.fn(() => Promise.resolve()),
    };

    scheduler.hook.store.find.mockResolvedValue([{
      name: 'name',
      repeatInterval: 10,
      data: {},
    }]);
    await repository.start(scheduler);
    expect(scheduler.hook.store.find).toHaveBeenCalled();
    expect(scheduler.start).toHaveBeenCalled();
  });

  test('stop', async () => {
    const scheduler = {
      stop: jest.fn(),
    };

    repository.stop(scheduler);
    expect(scheduler.stop).toHaveBeenCalled();
  });

  test('status', async () => {
    const scheduler = {
      processor: {
        options: {
          workerName: 'test',
        },
        isProcessing: false,
        nthRunner: 0,
      },
      running: false,
      registry: {
        jobs: [],
      },
    };

    const res = repository.status(scheduler);
    expect(res).toHaveProperty('schedulerWorkerName');
    expect(res).toHaveProperty('isSchedulerRunning');
    expect(res).toHaveProperty('isProcessing');
    expect(res).toHaveProperty('totalRunningMiles');
    expect(res).toHaveProperty('totalJobsInMemory');
  });
});
