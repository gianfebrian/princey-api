const nodeFetch = require('node-fetch');

const fetch = require('./../fetch');

jest.mock('node-fetch');

describe('fetch', () => {
  test.each([
    [
      ...(() => {
        const url = 'https://example.com/';
        const options = {
          method: 'post',
          timeout: 1000,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const data = { url };

        const expected = [
          new URL(url),
          options,
        ];

        return ['basic default to get', data, false, expected];
      })(),
    ],
    [
      ...(() => {
        const url = 'https://example.com/';
        const options = {
          method: 'get',
          timeout: 1000,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const data = {
          url,
          ...options,
          query: { test: 1 },
        };

        const expected = [
          new URL(`${url}?test=1`),
          options,
        ];

        return ['with query', data, false, expected];
      })(),
    ],
    [
      ...(() => {
        const url = 'https://example.com/:id/';
        const options = {
          method: 'post',
          timeout: 1000,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const data = {
          url,
          ...options,
          query: { test: 1 },
          params: { id: 1 },
        };

        const expected = [
          new URL(`${url.replace(':id', 1)}?test=1`),
          options,
        ];

        return ['with query and params', data, false, expected];
      })(),
    ],
    [
      ...(() => {
        const url = 'https://example.com/:id/';
        const options = {
          method: 'post',
          timeout: 1000,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const data = {
          url,
          ...options,
          query: { test: 1 },
          params: { id: 1 },
          body: { dummy: 'data' },
        };

        const expected = [
          new URL(`${url.replace(':id', 1)}?test=1`),
          Object.assign(options, { body: JSON.stringify({ dummy: 'data' }) }),
        ];

        return ['with query and params and body', data, false, expected];
      })(),
    ],
    [
      ...(() => {
        const url = 'https://example.com/:id/';
        const options = {
          method: 'post',
          timeout: 1000,
          headers: {
            'Content-Type': 'application/json',
            authorization: 'Bearer token',
          },
        };

        const data = {
          url,
          ...options,
          query: { test: 1 },
          params: { id: 1 },
          body: { dummy: 'data' },
        };

        const expected = [
          new URL(`${url.replace(':id', 1)}?test=1`),
          Object.assign(options, { body: JSON.stringify({ dummy: 'data' }) }),
        ];

        return ['with query and params, headers and body', data, false, expected];
      })(),
    ],
  ])(
    'exec() - %s', (title, data, error, expected) => {
      const fetcher = jest.fn();
      fetch.exec(fetcher)({ data });
      expect(fetcher).toHaveBeenCalledWith(...expected);
    },
  );

  test('fetcher default', () => {
    const data = { url: 'https://example.com' };
    fetch.exec()({ data });
    expect(nodeFetch).toHaveBeenCalled();
  });
});
