
const repository = require('./../repository');
const koa = require('./../koa');

jest.mock('./../fetch');
jest.mock('./../repository');

describe('koa', () => {
  test('middleware', () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {};
    koa.middleware(scheduler)(ctx, next);
    expect(ctx).toHaveProperty('scheduler');
    expect(next).toHaveBeenCalled();
  });

  test('list', async () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {
      scheduler,
    };

    const expected = { data: 'test' };
    repository.list.mockResolvedValue(expected);
    await koa.list(ctx, next);
    expect(ctx.body).toEqual(expected);
    expect(next).toHaveBeenCalled();
  });

  test('get', async () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {
      scheduler,
      params: {},
    };

    const expected = { data: 'test' };
    repository.get.mockResolvedValue(expected);
    await koa.get(ctx, next);
    expect(ctx.body).toEqual(expected);
    expect(next).toHaveBeenCalled();
  });

  test('upsert', async () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {
      scheduler,
      request: {
        body: {},
      },
    };

    const expected = 200;
    repository.upsert.mockResolvedValue(null);
    await koa.upsert(ctx, next);
    expect(ctx.status).toEqual(expected);
    expect(next).toHaveBeenCalled();
  });

  test('delete', async () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {
      scheduler,
      params: {},
    };

    const expected = 200;
    repository.delete.mockResolvedValue(null);
    await koa.delete(ctx, next);
    expect(ctx.status).toEqual(expected);
    expect(next).toHaveBeenCalled();
  });

  test('status', async () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {
      scheduler,
    };

    const expected = { data: 'test' };
    repository.status.mockReturnValue(expected);
    koa.status(ctx, next);
    expect(ctx.body).toEqual(expected);
    expect(next).toHaveBeenCalled();
  });

  test('start', async () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {
      scheduler,
    };

    const expected = 200;
    repository.start.mockResolvedValue(null);
    await koa.start(ctx, next);
    expect(ctx.status).toEqual(expected);
    expect(next).toHaveBeenCalled();
  });

  test('stop', async () => {
    const scheduler = jest.fn();
    const next = jest.fn();
    const ctx = {
      scheduler,
    };

    const expected = 200;
    repository.stop.mockReturnValue(null);
    koa.stop(ctx, next);
    expect(ctx.status).toEqual(expected);
    expect(next).toHaveBeenCalled();
  });

  test('router', () => {
    const router = koa.router([]);
    expect(router.opts.prefix).toEqual('/schedulers');
    [
      {
        path: '/schedulers(.*)',
        methods: [],
      },
      {
        path: '/schedulers/jobs',
        methods: ['HEAD', 'GET'],
      },
      {
        path: '/schedulers/jobs/:name',
        methods: ['HEAD', 'GET'],
      },
      {
        path: '/schedulers/jobs',
        methods: ['POST'],
      },
      {
        path: '/schedulers/jobs/:name',
        methods: ['DELETE'],
      },
      {
        path: '/schedulers/status',
        methods: ['HEAD', 'GET'],
      },
      {
        path: '/schedulers/controls/start',
        methods: ['HEAD', 'GET'],
      },
      {
        path: '/schedulers/controls/stop',
        methods: ['HEAD', 'GET'],
      },
    ].forEach((v, i) => {
      expect(router.stack[i].path).toEqual(v.path);
      expect(router.stack[i].methods).toEqual(v.methods);
    });
  });
});
