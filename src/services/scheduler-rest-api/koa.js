
const Router = require('koa-router');

const repository = require('./repository');

exports.middleware = (scheduler) => (ctx, next) => {
  ctx.scheduler = scheduler;
  return next();
};

exports.list = async (ctx, next) => {
  try {
    const data = await repository.list(ctx.scheduler);
    ctx.status = 200;
    ctx.body = data;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.get = async (ctx, next) => {
  try {
    const data = await repository.get(ctx.scheduler, ctx.params.name);
    ctx.status = 200;
    ctx.body = data || {};
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.upsert = async (ctx, next) => {
  try {
    await repository.upsert(ctx.scheduler, ctx.request.body);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.delete = async (ctx, next) => {
  try {
    await repository.delete(ctx.scheduler, ctx.params.name);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.status = (ctx, next) => {
  try {
    const data = repository.status(ctx.scheduler);
    ctx.status = 200;
    ctx.body = data;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    next();
  }
};

exports.start = async (ctx, next) => {
  try {
    await repository.start(ctx.scheduler);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    await next();
  }
};

exports.stop = async (ctx, next) => {
  try {
    repository.stop(ctx.scheduler);
    ctx.status = 200;
  } catch (err) {
    ctx.status = 400;
    ctx.state.error = err;
  } finally {
    next();
  }
};

exports.router = (middlewares) => {
  const router = new Router({ prefix: '/schedulers' });
  router.use(middlewares);

  router.get('/jobs', this.list);
  router.get('/jobs/:name', this.get);
  router.post('/jobs', this.upsert);
  router.delete('/jobs/:name', this.delete);
  router.get('/status', this.status);
  router.get('/controls/start', this.start);
  router.get('/controls/stop', this.stop);

  return router;
};
