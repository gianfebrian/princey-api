
const Registry = require('./registry');
const Processor = require('./processor');
const Hook = require('./adapters/mongoose/hook');
const Persistent = require('./adapters/mongoose/model');

class Scheduler {
  constructor({
    options = {},
    store = Persistent,
    hook = new Hook(),
    registry = new Registry(),
    processor = new Processor(),
    logger = console,
  }) {
    this.running = false;
    this.loopInterval = options.loopInterval || 5000;
    this.immediateRun = options.immediateRun || true;

    this.hook = hook;
    this.logger = logger;
    this.hook.logger = logger;
    this.store = store;
    this.hook.store = this.store.init(options.connection, options.collection);

    this.processor = processor;
    this.registry = registry;
    this.processor.registry = registry;
    this.processor.hook = hook;
    this.processor.options = options;

    this.timers = [];
  }

  create(name, processor) {
    this.logger.debug('registering job into registry', name);
    this.registry.register(name, processor);
  }

  remove(name) {
    this.logger.debug('removing job from registry', name);
    this.registry.unregister(name);
  }

  every(name, repeatInterval, data) {
    this.registry.setAttributes(name, {
      name,
      data,
      repeatInterval,
      type: 'single',
    });

    this.timers.push(this.hook.afterSetSchedule(this.registry.getByName(name)));
  }

  async start() {
    if (this.running) {
      return this;
    }

    await Promise.all(this.timers);

    // immediate run
    if (this.immediateRun) {
      await this.processor.iterate(new Date());
    }

    this.running = setInterval(() => this.processor.iterate(new Date()), this.loopInterval);

    return this;
  }

  stop() {
    clearInterval(this.running);
    this.running = false;
  }
}

module.exports = Scheduler;
