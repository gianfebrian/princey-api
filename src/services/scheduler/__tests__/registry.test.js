const Registry = require('./../registry');

describe('scheduler/registry', () => {
  test('Registry', () => {
    const registry = new Registry();
    expect(registry).toBeInstanceOf(Registry);

    // assert properties and methods
    [
      'jobs',
      'getByName',
      'reset',
      'setAttributes',
      'register',
      'unregister',
    ].forEach(expect(registry).toHaveProperty);
  });

  test.each([
    [
      'found an job',
      {
        name: 'registryName',
        jobs: {
          registryName: {},
        },
      },
      false,
      {},
    ],
    [
      'job not found',
      {
        name: 'missingRegistryName',
        jobs: {
          registryName: {},
        },
      },
      false,
      undefined,
    ],
  ])(
    'getByName() - %s', async (title, data, error, expected) => {
      const registry = new Registry();
      registry.jobs = { ...data.jobs };
      expect(registry.getByName(data.name)).toEqual(expected);
    },
  );

  test.each([
    [
      'with new registry',
      {
        jobs: {
          registryName: {},
        },
        newRegistry: {
          newRegistryName: {},
        },
      },
      false,
      {
        newRegistryName: {},
      },
    ],
    [
      'with new empty registry',
      {
        jobs: {
          registryName: {},
        },
        newRegistry: undefined,
      },
      false,
      {},
    ],
  ])(
    'reset() - %s', async (title, data, error, expected) => {
      const registry = new Registry();
      registry.jobs = { ...data.jobs };
      registry.reset(data.newRegistry);
      expect(registry.jobs).toEqual(expected);
    },
  );

  test.each([
    [
      'on registered job',
      {
        name: 'registryName',
        jobs: {
          registryName: {},
        },
        attributes: { data: {} },
      },
      false,
      {
        registryName: {
          data: {},
        },
      },
    ],
    [
      'on unknown job',
      {
        name: 'missingRegistryName',
        jobs: {
          registryName: {},
        },
        attributes: { data: {} },
      },
      false,
      {
        registryName: {},
      },
    ],
  ])(
    'setAttributes() - %s', async (title, data, error, expected) => {
      const registry = new Registry();
      registry.jobs = { ...data.jobs };
      registry.setAttributes(data.name, data.attributes);
      expect(registry.jobs).toEqual(expected);
    },
  );

  test.each([
    [
      'job does not already exist (create)',
      {
        name: 'missingRegistryName',
        jobs: {},
        processor: () => 'new job',
      },
      false,
      'new job',
    ],
    [
      'job already exists (update)',
      {
        name: 'registryName',
        jobs: {
          registryName: {},
          processor: () => { },
        },
        processor: () => 'updated job',
      },
      false,
      'updated job',
    ],
  ])(
    'register() - %s', async (title, data, error, expected) => {
      const registry = new Registry();
      registry.jobs = { ...data.jobs };
      registry.register(data.name, data.processor);
      expect(registry.jobs[data.name].processor()).toEqual(expected);
    },
  );

  test.each([
    [
      'remove existing job from registry',
      {
        name: 'registryName',
        jobs: {
          registryName: {},
        },
      },
      false,
      {},
    ],
    [
      'do nothing for missing job name',
      {
        name: 'missingRegistryName',
        jobs: {
          registryName: {},
        },
      },
      false,
      {
        registryName: {},
      },
    ],
  ])(
    'unregister() - %s', async (title, data, error, expected) => {
      const registry = new Registry();
      registry.jobs = { ...data.jobs };
      registry.unregister(data.name);
      expect(registry.jobs).toEqual(expected);
    },
  );
});
