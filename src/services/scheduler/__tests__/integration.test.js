const { MongoMemoryServer } = require('mongodb-memory-server');

const mongoose = require('mongoose');

const Scheduler = require('./../');

const mongod = new MongoMemoryServer();

const delay = (ms, obj = {}) => new Promise((res) => setTimeout(() => res(obj), ms));

const waitForCallTimes = (numberOfCall, jestFn) => (
  new Promise((resolve) => {
    const begin = Date.now();
    const timeStacks = [];
    let calls = 0;
    const intervalObj = setInterval(() => {
      if (jestFn.mock.calls.length !== calls) {
        const endItem = Date.now();
        timeStacks.push(endItem - begin);
        calls += 1;
      }

      if (jestFn.mock.calls.length === numberOfCall) {
        const end = Date.now();
        clearInterval(intervalObj);
        resolve({ timer: end - begin, timeStacks });
      }
    }, 1);
  })
);

// each job execution relies on the process loop interval
// if it is greater than repeat interval, job execution should follow the loop.
// say we set loop interval = 5s, even if we set repeat interval to 1s
// said job will be executed every 5s.
// if the process loop interval is less than repeat interval, the execution will
// follow repeat interval.
// say we set loop interval = 100ms, if we set repeat interval to 1s,
// said job will be executed every 1s.
//
// this threshold represents lower and upper bound of interval
// maximum delay tolerance with default 50ms
const getThreshold = (
  loopInterval,
  repeatInterval,
  delayInterval,
  expectedCall,
  tolerance = 50,
) => {
  const baseMin = loopInterval > repeatInterval
    ? loopInterval * (expectedCall - 1)
    : repeatInterval * (expectedCall - 1);

  const baseMax = baseMin + (delayInterval * expectedCall);

  const min = baseMin - tolerance;
  const max = baseMax + tolerance;

  return { min, max };
};

describe('Scheduler integration test', () => {
  beforeAll(async () => {
    const uri = await mongod.getUri();

    const connectionOption = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    };

    await mongoose.connect(uri, connectionOption);
  });

  afterAll(async () => {
    await mongoose.connection.db.dropDatabase();
    await mongoose.disconnect();
    await mongod.stop();
  });

  beforeEach(async () => {
    await mongod.ensureInstance();

    const collections = await mongoose.connection.db.collections();
    const removeAll = collections.reduce((out, col) => [...out, col.deleteMany({})], []);

    // reset mongoose models. mongoose reuses model initiation and stores it globally.
    mongoose.models = {};

    return Promise.all(removeAll);
  });

  test('default behaviour', async () => {
    const tolerance = 50;
    const delayInterval = 50;
    const repeatInterval = 1;
    const unitMultiplier = 1;
    const expected = 3;
    const unitMeasurement = 'milliseconds';

    const loopInterval = 1;

    const scheduler = new Scheduler({
      options: { loopInterval },
      logger: {
        debug: () => { },
        error: () => { },
      },
    });

    const processor = jest.fn(async () => {
      await delay(delayInterval);
      return Promise.resolve();
    });
    scheduler.create('testing', (job) => processor(job));
    scheduler.every('testing', `${repeatInterval} ${unitMeasurement}`, { testing: 1 });
    await scheduler.start();

    const { timer, timeStacks } = await waitForCallTimes(expected, processor);
    scheduler.stop();

    const threshold = getThreshold(
      loopInterval,
      repeatInterval * unitMultiplier,
      delayInterval,
      expected,
      tolerance,
    );
    expect(timeStacks[0]).toBeLessThanOrEqual(tolerance);
    expect(timer).toBeGreaterThanOrEqual(threshold.min);
    expect(timer).toBeLessThanOrEqual(threshold.max);
  });

  test('doc missing while processing', async () => {
    const tolerance = 50;
    const delayInterval = 50;
    const repeatInterval = 1;
    const unitMultiplier = 1;
    const expected = 3;
    const unitMeasurement = 'milliseconds';

    const loopInterval = 1;

    const scheduler = new Scheduler({
      options: { loopInterval },
      logger: {
        debug: () => { },
        error: () => { },
      },
    });

    const { store } = scheduler.hook;

    const processor = jest.fn(async () => {
      await delay(delayInterval);
      return Promise.resolve();
    });
    scheduler.create('testing', (job) => processor(job));
    scheduler.every('testing', `${repeatInterval} ${unitMeasurement}`, { testing: 1 });

    let updateOneCalled = 0;
    scheduler.hook.store.updateOne = jest.fn(() => {
      updateOneCalled += 1;

      if (updateOneCalled === 1) {
        return Promise.resolve({ n: 0 });
      }

      return store.updateOne;
    });

    let findOneCalled = 0;
    scheduler.hook.store.findOne = jest.fn(() => {
      findOneCalled += 1;

      if (findOneCalled === 2) {
        return Promise.resolve(null);
      }

      return store.findOne;
    });
    await scheduler.start();

    const { timer } = await waitForCallTimes(expected, processor);
    scheduler.stop();

    const threshold = getThreshold(
      loopInterval,
      repeatInterval * unitMultiplier,
      delayInterval,
      expected,
      tolerance,
    );

    expect(timer).toBeGreaterThanOrEqual(threshold.min);
    expect(timer).toBeLessThanOrEqual(threshold.max);
  });

  test('optimistic concurrency control', async () => {
    const delayInterval = 50;
    const repeatInterval = 100;
    const expected = 3;
    const unitMultiplier = 1;
    const unitMeasurement = 'milliseconds';
    const maxLockWait = 0;

    const loopInterval = 100;

    const createSchedulerContainer = async (
      containers,
      watcher,
      errorWatcher,
      data = {},
      spawnDelay = 5,
    ) => {
      // to create psuedo-deterministic sequence
      await delay(spawnDelay);

      const scheduler = new Scheduler({
        options: { loopInterval, maxLockWait },
        connection: mongoose.connection,
        logger: {
          debug: () => { },
          error: errorWatcher,
        },
      });

      const processor = jest.fn(async () => {
        await delay(delayInterval);
        return Promise.resolve();
      });

      scheduler.create('testing', (job) => processor(job));
      scheduler.every('testing', `${repeatInterval} ${unitMeasurement}`, data);
      containers.push(scheduler.start());

      const res = await waitForCallTimes(expected, processor);
      scheduler.stop();
      watcher(res);

      return res;
    };

    const schedulers = [];
    const watcher = jest.fn();
    const errorWatcher = jest.fn();
    const numberOfContainer = 4; // number of concurrency
    const containers = [];
    for (let i = 0; i < numberOfContainer; i += 1) {
      schedulers.push(
        createSchedulerContainer(containers, watcher, errorWatcher, { i }, 5),
      );
    }

    await Promise.all(containers);
    await Promise.all(schedulers);
    const orderedSequence = watcher.mock.calls
      .map((calls) => calls[0])
      .map((res) => res.timeStacks)
      .flat()
      .sort((x, y) => x - y);

    orderedSequence.sort((after, before) => expect(after).toBeGreaterThan(before));
    orderedSequence.sort((after, before) => {
      const tolerance = 50;
      const diff = after - before;

      expect(diff).toBeGreaterThanOrEqual(
        0,
        // sometime it is failed. compare to zero for now
        // (repeatInterval * unitMultiplier - tolerance),
      );

      expect(diff).toBeLessThanOrEqual(
        (repeatInterval * unitMultiplier) + delayInterval + tolerance,
      );

      return 0;
    });
  }, 120000);
});
