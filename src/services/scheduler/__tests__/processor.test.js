const moment = require('moment');

const Registry = require('./../registry');
const Processor = require('./../processor');

const mockHook = {
  beforeInitProcess: jest.fn(),
  errorInitProcess: jest.fn(),
  afterSetSchedule: jest.fn(),
  beforeJobProcess: jest.fn(),
  afterJobProcess: jest.fn(),
  errorJobProcess: jest.fn(),
};

describe('scheduler/processor', () => {
  test('Processor', () => {
    const registry = new Registry();
    const processor = new Processor(registry);
    expect(processor).toBeInstanceOf(Processor);

    // assert properties and methods
    expect(processor.registry).toBeInstanceOf(Registry);
    expect(Processor.parseDuration).toBeInstanceOf(Function);

    [
      'runAndPrepareNext',
      'process',
      'iterate',
    ].forEach(expect(processor).toHaveProperty);
  });

  test.each([
    ['valid format', '1 minute', false, { duration: 1, unit: 'minute' }],
    ['duration not a number', 'x minute', true, 'invalid duration'],
    ['missing unit', '1', true, 'invalid duration'],
    ['missing duration', 'minute', true, 'invalid duration'],
    ['empty', undefined, true, 'invalid duration'],
  ])(
    'parseDuration() - %s', (title, data, error, expected) => {
      if (error) {
        return expect(() => {
          Processor.parseDuration(data);
        }).toThrow(expected);
      }

      return expect(Processor.parseDuration(data)).toEqual(expected);
    },
  );

  test.each([
    [
      'registry has no nextRunAt property',
      {
        name: 'registryName',
        jobs: {
          registryName: {
            processor: () => { },
          },
        },
        nextTime: 1,
      },
      false,
      1,
    ],
    [
      'registry has nextRunAt property',
      {
        name: 'registryName',
        jobs: {
          registryName: {
            processor: () => { },
            nextRunAt: 0,
          },
        },
        nextTime: 1,
      },
      false,
      1,
    ],
  ])(
    'runAndPrepareNext() - %s', async (title, data, error, expected) => {
      const registry = new Registry();
      registry.jobs = { ...data.jobs };
      const processor = new Processor(registry, mockHook, {});
      await processor.runAndPrepareNext(registry.jobs[data.name], new Date(), data.nextTime);
      expect(registry.jobs[data.name].nextRunAt).toEqual(expected);
    },
  );

  test.each([
    [
      'job has no nextRunAt property (the first run)',
      {
        name: 'registryName',
        jobs: {
          registryName: {
            processor: () => 'executed',
            repeatInterval: '1 minute',
          },
        },
      },
      false,
      1,
    ],
    [
      'job has nextRunAt property (subsequent run)',
      {
        name: 'registryName',
        jobs: {
          registryName: {
            processor: () => 'executed',
            repeatInterval: '1 minute',
          },
        },
        presetNextRunAtInterval: [-1, 'minute'],
      },
      false,
      1,
    ],
    [
      'job not found',
      {
        name: 'missingRegistryName',
        jobs: {
          registryName: {
            processor: () => 'executed',
            repeatInterval: '1 minute',
          },
        },
      },
      false,
      0,
    ],
  ])(
    'process() - %s', async (title, data, error, expected) => {
      const now = moment();

      const registry = new Registry();
      registry.jobs = { ...data.jobs };


      const nextRunAt = data.presetNextRunAtInterval
        && now.add(...data.presetNextRunAtInterval);

      if (registry.jobs[data.name]) {
        registry.jobs[data.name].nextRunAt = nextRunAt;
      }

      const processor = new Processor(registry);
      processor.runAndPrepareNext = jest.fn();
      processor.hook = mockHook;
      await processor.process(registry.jobs[data.name], new Date());

      expect(processor.runAndPrepareNext).toHaveBeenCalledTimes(expected);
    },
  );

  test.each([
    [
      'registry is not empty',
      {
        jobs: {
          registryName: {
            processor: () => { },
            repeatInterval: '1 minute',
          },
        },
      },
      false,
      1,
    ],
    [
      'more than 1 job',
      {
        jobs: {
          registryName: {
            processor: () => { },
            repeatInterval: '1 minute',
          },
          registryName2: {
            processor: () => { },
            repeatInterval: '1 minute',
          },
        },
      },
      false,
      2,
    ],
    [
      'registry is empty',
      { jobs: {} },
      false,
      0,
    ],
  ])(
    'iterate() - %s', async (title, data, error, expected) => {
      const registry = new Registry();
      registry.jobs = { ...data.jobs };

      const processor = new Processor(registry);
      processor.process = jest.fn();
      processor.iterate(new Date());

      expect(processor.process).toHaveBeenCalledTimes(expected);
    },
  );
});
