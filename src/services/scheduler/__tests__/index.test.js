const Registry = require('./../registry');
const Processor = require('./../processor');
const Scheduler = require('./../index');

const mockHook = {
  beforeInitProcess: jest.fn(),
  errorInitProcess: jest.fn(),
  afterSetSchedule: jest.fn(),
  beforeJobProcess: jest.fn(),
  afterJobProcess: jest.fn(),
  errorJobProcess: jest.fn(),
};

describe('scheduler', () => {
  test('Scheduler', () => {
    const scheduler = new Scheduler({
      hook: mockHook,
      logger: {
        debug: () => { },
        error: () => { },
      },
    });
    expect(scheduler).toBeInstanceOf(Scheduler);
    expect(scheduler.registry).toBeInstanceOf(Registry);
    expect(scheduler.processor).toBeInstanceOf(Processor);
  });

  test('create()', () => {
    const scheduler = new Scheduler({
      hook: mockHook,
      logger: {
        debug: () => { },
        error: () => { },
      },
    });
    scheduler.registry.register = jest.fn();
    scheduler.create('name', () => {});
    expect(scheduler.registry.register).toHaveBeenCalled();
  });

  test('remove()', () => {
    const scheduler = new Scheduler({
      hook: mockHook,
      logger: {
        debug: () => { },
        error: () => { },
      },
    });
    scheduler.registry.unregister = jest.fn();
    scheduler.remove('name');
    expect(scheduler.registry.unregister).toHaveBeenCalled();
  });

  test('every()', () => {
    const scheduler = new Scheduler({
      hook: mockHook,
      logger: {
        debug: () => { },
        error: () => { },
      },
    });
    scheduler.registry.setAttributes = jest.fn();
    scheduler.every('name', '1 minute', {});
    // assert data?
    expect(scheduler.registry.setAttributes).toHaveBeenCalled();
  });

  test('start()', async () => {
    const scheduler = new Scheduler({
      hook: mockHook,
      logger: {
        debug: () => { },
        error: () => { },
      },
    });
    scheduler.processor.iterate = jest.fn();
    await scheduler.start();
    expect(scheduler.processor.iterate).toHaveBeenCalledTimes(1);
    await scheduler.start();
    expect(scheduler.processor.iterate).toHaveBeenCalledTimes(1);
    clearInterval(scheduler.running);
  });
});
