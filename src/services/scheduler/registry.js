
class Registry {
  constructor() {
    this.jobs = {};
  }

  getByName(name) {
    return this.jobs[name];
  }

  reset(newRegistry = {}) {
    this.jobs = newRegistry;
  }

  setAttributes(name, attributes) {
    if (this.getByName(name)) {
      this.jobs[name] = { ...this.getByName(name), ...attributes };
    }
  }

  register(name, processor) {
    this.jobs[name] = { ...this.getByName(name), name, processor };
  }

  unregister(name) {
    const { [name]: _, ...newRegistry } = this.jobs;
    this.reset(newRegistry);
  }
}

module.exports = Registry;
