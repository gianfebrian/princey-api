const moment = require('moment');

class Processor {
  constructor(registry, hook, options) {
    this.registry = registry;
    this.hook = hook;
    this.options = options;
    this.nthRunner = 0;
    this.isProcessing = false;
  }

  // should validate and throw error?
  static parseDuration(durationText = '') {
    // get time duration
    // always assume repeatInterval in format "%d %s" with integer interval and unit of time
    // e.g, second, minute, hour, day, etc.
    const validUnits = [
      'millisecond',
      'milliseconds',
      'second',
      'seconds',
      'minute',
      'minutes',
      'hour',
      'hours',
      'day',
      'days',
      'week',
      'weeks',
      'month',
      'months',
      'year',
      'years',
    ];

    const splitInterval = durationText.split(' ');
    const duration = splitInterval[0] && Number(splitInterval[0]);
    const unit = splitInterval[1];

    if ((!duration || typeof duration !== 'number') || !validUnits.includes(unit)) {
      throw new Error('invalid duration');
    }

    return { duration, unit };
  }

  async runAndPrepareNext(ctx, now, nextTime) {
    try {
      ctx.lockedAt = now;
      ctx.nextRunAt = nextTime;

      await this.hook.beforeJobProcess(ctx);
      await ctx.processor(ctx);
      await this.hook.afterJobProcess(ctx);

      // if no hook is provided still need to reset this value
      ctx.lockedAt = null;
      ctx.failedAt = null;
      ctx.retryAttempt = 0;
    } catch (err) {
      const diff = moment(now).utc().diff(moment(ctx.lockedAt || undefined).utc(), 'milliseconds');

      const { maxLockWait = 10 * 60 * 1000, maxRetry = 3 } = this.options;

      ctx.retryAttempt = ctx.retryAttempt || 0;
      if (ctx.retryAttempt <= maxRetry && diff >= maxLockWait) {
        ctx.retryAttempt += 1;
        ctx.lockedAt = null;
      }

      ctx.failedAt = now;

      await this.hook.errorJobProcess(err, ctx);

      throw err;
    }
  }

  // Job processor determines whether the job needs to be executed.
  // And pick the right processor by its name from the registry.
  async process(ctx, parentNow) {
    const now = moment(parentNow).utc().toDate();

    try {
      if (!ctx) {
        return ctx;
      }

      await this.hook.beforeInitProcess(ctx);
      const { duration, unit } = this.constructor.parseDuration(ctx.repeatInterval);
      const nextTime = moment(ctx.nextRunAt || now).utc().add(duration, unit).toDate();

      // Determine whether it is the first run for the job
      // the first run is indicated by nextRunAt with no value.
      // It sets the computed time to nextRunAt afterwards.
      if (!ctx.nextRunAt) {
        await this.runAndPrepareNext(ctx, now, now);
        return ctx;
      }

      // Determine whether it is the subsequent run for the job
      // and whether the job is eligible to be executed
      // by comparing time difference between nextRunAt and now.
      // Eligibility is determined by whether the difference is greater or equal
      // with the preferred interval.
      // It sets the computed time to nextRunAt afterwards.
      const diff = moment(parentNow).utc().diff(moment(ctx.nextRunAt).utc(), unit);
      if (diff >= duration) {
        await this.runAndPrepareNext(ctx, now, nextTime);
        return ctx;
      }
    } catch (err) {
      await this.hook.errorInitProcess(err, ctx);
    }

    // error has been taken care of above
    // since ctx should be containing lock info, return it.
    return ctx;
  }

  async createContext(name, now) {
    const ctx = { ...this.registry.getByName(name) };
    const nextCtx = await this.process(ctx, now);
    this.registry.setAttributes(name, nextCtx);

    return this.registry.getByName(name);
  }

  async iterate(now) {
    if (this.isProcessing) {
      return Promise.resolve();
    }

    const contextProcessors = Object.keys(this.registry.jobs)
      .reduce((jobs, name) => (
        [...jobs, this.createContext(name, now)]
      ), []);

    this.isProcessing = true;
    this.nthRunner += 1;

    const runningProcesses = await Promise.all(contextProcessors);

    this.isProcessing = false;

    return runningProcesses;
  }
}

module.exports = Processor;
