
class Hook {
  constructor(store, logger) {
    this.store = store;
    this.logger = logger;
  }

  async beforeInitProcess(ctx) {
    this.logger.debug('before initializing the job', ctx);

    const currentDocState = await this.store.findOne({ name: ctx.name });

    ctx.nextRunAt = currentDocState.nextRunAt;
    ctx.lockedAt = currentDocState.lockedAt;
    ctx.failedAt = currentDocState.failedAt;
    ctx.retryAttempt = currentDocState.retryAttempt;

    return currentDocState;
  }

  errorInitProcess(err, ctx) {
    this.logger.error('error initializing the job', err, ctx);
  }

  afterSetSchedule(ctx) {
    this.logger.debug('after job schedule being set', ctx);

    const {
      processor,
      nextRunAt,
      lockedAt,
      failedAt,
      retryAttempt,
      ...theJob
    } = ctx;

    return this.store.updateOne(
      {
        name: ctx.name,
      },
      theJob,
      {
        upsert: true,
      },
    );
  }

  async beforeJobProcess(ctx) {
    this.logger.debug('before processing the job', ctx);

    const res = await this.store.updateOne(
      {
        name: ctx.name,
        lockedAt: null,
      },
      {
        lockedAt: ctx.lockedAt,
      },
    );

    if (res && res.n === 0) {
      const currentDocState = await this.store.findOne({ name: ctx.name });
      if (!currentDocState) {
        throw new Error('job not found');
      }

      ctx.lockedAt = currentDocState.lockedAt;
      ctx.failedAt = currentDocState.failedAt;
      ctx.retryAttempt = currentDocState.retryAttempt;

      throw new Error('job is locked');
    }
  }

  async afterJobProcess(ctx) {
    this.logger.debug('after processing the job', ctx);

    return this.store.updateOne(
      {
        name: ctx.name,
        lockedAt: ctx.lockedAt,
      },
      {
        nextRunAt: ctx.nextRunAt,
        lockedAt: null,
        failedAt: null,
        retryAttempt: 0,
      },
    );
  }

  async errorJobProcess(err, ctx) {
    this.logger.error('error processing the job', err, ctx);

    return this.store.updateOne(
      {
        name: ctx.name,
      },
      {
        lockedAt: ctx.lockedAt,
        nextRunAt: ctx.nextRunAt,
        failedAt: ctx.failedAt,
        retryAttempt: ctx.retryAttempt,
      },
    );
  }
}

module.exports = Hook;
