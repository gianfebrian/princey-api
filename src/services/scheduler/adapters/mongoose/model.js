
const mongoose = require('mongoose');

const schedulerSchema = new mongoose.Schema({
  name: { type: String, index: true },
  type: { type: String },
  repeatInterval: { type: String },
  data: { type: Object },
  lockedAt: { type: Date, index: true },
  nextRunAt: { type: Date },
  failedAt: { type: Date },
});

exports.init = (connection = mongoose, collection = 'Scheduler') => (
  connection.model(collection, schedulerSchema)
);
