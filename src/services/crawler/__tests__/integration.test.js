
const fs = require('fs');
const path = require('path');
const http = require('http');
const { MongoMemoryServer } = require('mongodb-memory-server');

const mongoose = require('mongoose');

const Crawler = require('./../');
const htmlConfig = require('./configs/html.json');
const headlessConfig = require('./configs/headless.json');

const mongod = new MongoMemoryServer();

describe('crawler', () => {
  beforeAll(async () => {
    const uri = await mongod.getUri();

    const connectionOption = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    };

    await mongoose.connect(uri, connectionOption);
  });

  afterAll(async () => {
    await mongoose.connection.db.dropDatabase();
    await mongoose.disconnect();
    await mongod.stop();
  });

  beforeEach(async () => {
    await mongod.ensureInstance();

    const collections = await mongoose.connection.db.collections();
    const removeAll = collections.reduce((out, col) => [...out, col.deleteMany({})], []);

    // reset mongoose models. mongoose reuses model initiation and stores it globally.
    mongoose.models = {};

    return Promise.all(removeAll);
  });

  test('parse dummy html', async () => {
    const htmlFilePath = path.join(__dirname, 'dummy', 'example.html');
    const htmlFile = fs.readFileSync(htmlFilePath);
    const server = http.createServer((req, res) => {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write(htmlFile);
      res.end();
    }).listen();

    server.on('error', () => {
      server.close();
    });

    const url = `http://localhost:${server.address().port}`;
    htmlConfig.sites = [url];

    try {
      const crawler = new Crawler({
        options: { connection: mongoose.connection },
      });

      await crawler.setConfig(htmlConfig);
      const config = await crawler.getConfigByUrl(url);
      const result = await crawler.fetch(url, config);
      expect(result.price).toBeGreaterThan(0);
      expect(result.description.length).toBeGreaterThan(0);
      expect(result.images.length).toBeGreaterThan(2);
    } finally {
      server.close();
    }
  });

  test('parse dummy headless', async () => { // eslint-disable-line
    const htmlFilePath = path.join(__dirname, 'dummy', 'example.html');
    const htmlFile = fs.readFileSync(htmlFilePath);
    const server = http.createServer((req, res) => {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write(htmlFile);
      res.end();
    }).listen();

    server.on('error', () => {
      server.close();
    });

    const url = `http://localhost:${server.address().port}`;
    headlessConfig.sites = [url];

    try {
      const crawler = new Crawler({
        options: { connection: mongoose.connection },
      });

      await crawler.setConfig(headlessConfig);
      const config = await crawler.getConfigByUrl(url);
      const result = await crawler.fetch(url, config);
      expect(result.price).toBeGreaterThan(0);
      expect(result.description.length).toBeGreaterThan(0);
      expect(result.images.length).toBeGreaterThan(2);
    } finally {
      server.close();
    }
  }, 120000);
});
