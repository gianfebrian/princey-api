const html = require('./html');
const headless = require('./headless');

module.exports = { html, headless };
