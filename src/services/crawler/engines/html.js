
const fetch = require('node-fetch');
const parse5 = require('parse5');
const xmlserializer = require('xmlserializer');
const { DOMParser } = require('xmldom');
const xpath = require('xpath');

const sanitizeHtml = require('sanitize-html');

exports.parse = async (url, config) => {
  const res = await fetch(url, {
    timeout: 5000,
  });

  const html = await res.text();

  const $ = this.load(html);

  const output = {};
  Object.keys(config.attributes).forEach((key) => {
    this.setOutput($, config.attributes[key], output, key);
  });

  return output;
};

exports.load = (html) => {
  const document = parse5.parse(html.toString());

  const xhtml = xmlserializer.serializeToString(document);
  const doc = new DOMParser().parseFromString(xhtml);
  const select = xpath.useNamespaces({ x: 'http://www.w3.org/1999/xhtml' });

  return this.select(select, doc);
};

exports.select = (select, doc) => (
  (selector, ns = false) => {
    const injectNs = ns ? selector.replace(/\/(?!\/|@)/g, '/x:') : selector;
    const results = select(injectNs, doc);

    return results;
  }
);

exports.setOutput = ($, attribute, ctx, key) => {
  const value = this.getValueFromSelectors($, attribute);

  if (attribute.output === 'multiple') {
    ctx[key] = value;

    return ctx;
  }

  [ctx[key]] = value;

  return ctx;
};

exports.getValueFromSelectors = ($, attribute) => {
  const output = [];

  for (let i = 0; i < attribute.selectors.length; i += 1) {
    const value = $(attribute.selectors[i].selector)
      .map((v) => {
        const currentV = this.get(attribute.selectors[i].type, v);

        return this.cast(attribute.target, currentV);
      });

    if (value) {
      const outputItem = Array.isArray(value) ? value : [value];
      output.push(...outputItem);
    }

    if (output.length >= attribute.max) {
      return output.slice(0, attribute.max);
    }
  }

  return output;
};

exports.get = (type, el) => {
  switch (type) {
    case 'attr':
      return el.nodeValue;
    case 'text':
      return el.textContent;
    case 'html':
      return sanitizeHtml(el.toString());
    default:
      return el.nodeValue;
  }
};

exports.cast = (type, value) => {
  switch (type) {
    case 'number':
      return Number(value || 0) || 0;
    case 'boolean':
      return (value || '').toLowerCase === 'false' || true;
    case 'string':
      return (value || '').trim().toString();
    default:
      return value;
  }
};
