
const persistent = require('./mongoose-persistent');
const queue = require('./queue');
const Context = require('./context');

class SimpleQueue {
  constructor({ options }) {
    this.persistent = persistent.init(options.connection, options.collection);
    this.queue = queue;
    this.uncommitedThreshold = options.uncommitedThreshold;
  }

  createNewContext() {
    return new Context(this);
  }

  push(data) {
    return this.queue.push(this.persistent, data);
  }

  pull() {
    return this.queue.pull(this.persistent);
  }

  commit({ _id }) {
    return this.queue.commit(this.persistent, { _id });
  }

  rollback({ _id }) {
    return this.queue.rollback(this.persistent, { _id });
  }

  getUncommitedQueues() {
    return this.queue.getUncommitedQueues(this.persistent, this.uncommitedThreshold);
  }
}

module.exports = SimpleQueue;
