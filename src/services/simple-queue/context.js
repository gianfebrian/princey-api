
class Context {
  constructor(simpleQueue) {
    this.simpleQueue = simpleQueue;
    this.queue = {};
  }

  push(data) {
    return this.simpleQueue.push(data);
  }

  async pull() {
    this.queue = await this.simpleQueue.pull();

    return this.queue;
  }

  commit() {
    return this.simpleQueue.commit(this.queue);
  }

  rollback() {
    return this.simpleQueue.rollback(this.queue);
  }
}

module.exports = Context;
