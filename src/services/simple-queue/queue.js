
exports.wrap = (data) => ({
  data,
  pulledAt: null,
  commitedAt: null,
  timestamp: Date.now(),
});

exports.push = (persistent, data) => (
  persistent.create(this.wrap(data))
);

// get fifo
exports.pull = (persistent) => (
  persistent.findOneAndUpdate(
    {
      pulledAt: null,
    },
    {
      $set: {
        pulledAt: Date.now(),
      },
    },
    {
      sort: { timestamp: 1 },
      new: true,
    },
  )
);

exports.commit = (persistent, { _id }) => (
  persistent.updateOne({ _id }, { $set: { commitedAt: Date.now() } })
);

exports.rollback = (persistent, { _id }) => (
  persistent.updateOne({ _id }, { $set: { pulledAt: null } })
);

exports.getUncommitedQueues = (persistent, threshold = 10 * 60 * 1000) => (
  persistent.find({
    pulledAt: { $gte: Date.now - threshold },
    commitedAt: null,
  })
);
