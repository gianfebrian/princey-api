
const mongoose = require('mongoose');

const bucketSchema = new mongoose.Schema({
  data: { type: Object },
  pulledAt: { type: Number },
  commitedAt: { type: Number },
  timestamp: { type: Number, index: true },
});

exports.init = (connection = mongoose, collection = 'QueueBucket') => (
  connection.model(collection, bucketSchema)
);
